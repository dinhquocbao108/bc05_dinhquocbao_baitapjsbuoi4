/**
 * input:
 * -tạo 3 biến cho người dùng nhập vào 3 số nguyên và lấy giá trị 
 * -dùng phép so sánh và, hoặc để giải bài toán 
 * -output ra kết quả.
 */
function sapXep(){
    var a = document.getElementById("number1").value*1;
    var b = document.getElementById("number2").value*1;
    var c = document.getElementById("number3").value*1;
    var beNhat = 0;
    var giua = 0;
    var lonNhat = 0;
        if (a <= b && a <= c) {
          beNhat = a;
          if (b <= c) {
            lonNhat = c;
            giua = b;
          }else{
            lonNhat = b;
            giua = c;
          }
        }else if (b <= c && b <= a) {
          beNhat = b;
          if (a <= c) {
            lonNhat = c;
            giua = a;
          }else{
             lonNhat = a;
            giua = c;
          }
        }else if (c <= a && c <= b) {
          beNhat = c;
          if (a <= b) {
            lonNhat = b;
            giua = a;
          }else{
              lonNhat = a;
            giua = b;
          }
        }
    document.getElementById("result").innerHTML=`
    <p>số có giá trị bé nhất: ${beNhat}</p>
    <p>số có giá trị bé thứ 2: ${giua}</p>
    <p>số có giá trị lớn nhất: ${lonNhat}</p>
    `;
}
